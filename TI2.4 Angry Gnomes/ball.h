#pragma once

#include "gameobject.h"
#include "gameobjectcomponent.h"
#include "objectmodel.h"
#include "physics.h"
#include "reactphysics3d.h"

class Ball : public GameObject
{
private:
	Vec3f initialPosition;
	BallPhysicsComponent* physics;
	DynamicsWorld* world;

public:
	Ball(rp3d::DynamicsWorld* world, Vec3f initialPos);
	~Ball() {};
	void launch(float launchX, float launchY);
	void reset();
};
