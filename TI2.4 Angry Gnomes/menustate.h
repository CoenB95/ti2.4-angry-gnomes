#pragma once
#include "gamestate.h"
#include "text.h"

class SelectionRectangle;

class MenuState : public GameState {
private:
	rp3d::DynamicsWorld & physicsWorld;
	int choice;
	float itemSpacing = 2.0f;
	SelectionRectangle* selectionRect;
	Text* titleText;
	Text* startText;
	Text* settingsText;
	Text* quitText;
	Text* versionText;

	enum MenuStatesEnum
	{
		Start = 0,
		Settings = 1,
		Quit = 2,
	};

public:
	MenuState(rp3d::DynamicsWorld & physicsWorld);
	void onInit() override;
	void onKeyPressed(unsigned char key) override;
	void onKeyCodePressed(int key) override;
	void selectUp();
	void selectDown();
	void enterScreen(int choice);
	void exitSettings();
	void onExit() override;
	void onUpdate(float elapsedSeconds) override;
};

class SelectionRectangle : public GameObject
{
public:
	float width;
	float height;

	SelectionRectangle(float width, float height);
};

class RectangleFillDrawComponent : public DrawComponent
{
public:
	void draw() override;
};

class RectangleOutlineDrawComponent : public DrawComponent
{
public:
	void draw() override;
};
