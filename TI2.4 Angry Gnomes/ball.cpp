#include <string>

#include "ball.h"
#include "gameobject.h"
#include "modeldrawcomponent.h"
#include "objectmodel.h"
#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES

#include "physics.h"
#include "reactphysics3d.h"
#include <cmath>

using namespace std;

Ball::Ball(rp3d::DynamicsWorld* world, Vec3f initialPos) : GameObject(),
world(world),
initialPosition(initialPos)
{
	addComponent(new ModelDrawComponent("models/ball/soccer_ball.obj"));
	reset();
}

void Ball::launch(float x, float y) 
{
	float forceLength = 50000000.0f;
	Vec3f force(sinf(y)*forceLength, sinf(x)*forceLength, -cosf(y)*forceLength);
	physics->applyForce(force);
}

void Ball::reset()
{
	if (physics != nullptr)
	{
		removeComponent(physics);
		physics = nullptr;
	}

	physics = new BallPhysicsComponent(world, initialPosition, 2.5f);
	addComponent(physics);
}
