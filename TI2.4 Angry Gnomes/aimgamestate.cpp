#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES

#include <GL/freeglut.h>
#include <cmath>
#include <thread>

#include "aimgamestate.h"
#include "ball.h"
#include "followcomponent.h"
#include "followballgamestate.h"
#include "gameobjectcomponent.h"
#include "gamestate.h"
#include "motiondetection.h"
#include "spectatorgamestate.h"
#include "vec.h"

AimGameState::AimGameState(Ball* ball) : GameState()
{
	
}

AimGameState::~AimGameState()
{
	thrd.join();
}

Mat AimGameState::cameraImage;
mutex AimGameState::cameraImageLock;

void motionThread(bool& stop, float& targetRotateX, float& targetRotateY, bool& launch)
{
	cout << "Thread start" << endl;
	MotionDetection detector;
	VideoCapture* capture = detector.motionInit();

	if (capture == nullptr)
	{
		cout << "No camera, thread end" << endl;
		return;
	}

	while (!stop)
	{
		Mat frame;
		*capture >> frame;
		DetectionResult result = detector.detectMotion(frame);
		if (result.handposture != POSTURE_NONE) {
			if (result.handposture == POSTURE_FIST) {
				targetRotateY = result.deltaX * 0.2f;
				targetRotateX = result.deltaY * 0.2f;
			} else {
				launch = true;
			}
			cout << "Thread result" << endl;
		}
		AimGameState::cameraImageLock.lock();
		AimGameState::cameraImage = frame;
		AimGameState::cameraImageLock.unlock();
	}

	detector.releaseResources();
	cout << "Thread end" << endl;
}

void AimGameState::onExit()
{
	stopMotionDetection = true;
}

void AimGameState::onInit()
{	
	targetRotateX = 0;
	targetRotateY = 0;
	manager->getCamera()->setType(Camera::CAMERA_TYPE_THIRD_PERSON);
	manager->getCamera()->thirdPersonTargetDistance = 16.0f;
	manager->getCamera()->addComponent(FollowComponent::translating(manager->ball, 0.9f));

	stopMotionDetection = false;
	thrd = thread(&motionThread, ref(stopMotionDetection), ref(targetRotateX), ref(targetRotateY), ref(shouldLaunch));
}

void AimGameState::onKeyCodePressed(int keyCode)
{
	GameState::onKeyCodePressed(keyCode);
}

void AimGameState::onKeyPressed(unsigned char key)
{
	GameState::onKeyPressed(key);

	if (key == 'f')
	{
		manager->changeState(new SpectatorGameState());
	}
	else if (key == ' ')
	{
		shouldLaunch = true;
	}
}

void AimGameState::onUpdate(float elapsedSeconds)
{
	GameState::onUpdate(elapsedSeconds);

	if (shouldLaunch)
	{
		shouldLaunch = false;
		manager->ball->launch(targetRotateX / 180.0f * M_PI, targetRotateY / 180.0f * M_PI);
		manager->changeState(new FollowBallGameState(manager->levelCenter, 0.99f, 150.0f, true));
	}

	float factor = 0.95f;
	rotationX = factor * rotationX + (1 - factor) * targetRotateX;
	rotationY = factor * rotationY + (1 - factor) * targetRotateY;

	manager->getCamera()->rotation =
		Quaternion::fromEulerAngles(rotationX / 180.0f * M_PI, 0.0f, 0.0f) *
		Quaternion::fromEulerAngles(0.0f, rotationY / 180.0f * M_PI, 0.0f);

	AimGameState::cameraImageLock.lock();
	if (!AimGameState::cameraImage.empty())
		imshow("result", AimGameState::cameraImage);
	AimGameState::cameraImageLock.unlock();
}
