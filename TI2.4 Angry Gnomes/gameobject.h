#pragma once

#include <vector>

#include "reactphysics3d.h"
#include "vec.h"

using namespace rp3d;
using namespace std;

class DrawComponent;
class GameObjectComponent;
class Rotation;

class GameObject
{
private:
	DrawComponent* drawComponent;
	vector<GameObjectComponent*> components;

public:
	vec::Vec3f position;
	Quaternion rotation = Quaternion::identity();

	GameObject();
	virtual ~GameObject();
	void addComponent(GameObjectComponent* component);
	void draw();
	void removeAllComponents();
	void removeDrawComponent();
	void removeComponent(GameObjectComponent* component);
	virtual void update(float elapsedSeconds);
};
