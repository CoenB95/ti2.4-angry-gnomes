#include "gameobject.h"
#include "gameobjectcomponent.h"
#include "modeldrawcomponent.h"
#include "objectmodel.h"

std::map<string, ObjModel*> ModelDrawComponent::cache;


ModelDrawComponent::ModelDrawComponent(const string fileName) : DrawComponent()
{
	if (cache.find(fileName) == cache.end())
		cache[fileName] = new ObjModel(fileName);
	model = cache[fileName];
}

void ModelDrawComponent::draw()
{
	model->draw();
}