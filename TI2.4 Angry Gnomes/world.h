#pragma once

#include "gameobject.h"
#include "gameobjectcomponent.h"
#include "objectmodel.h"

class World : public GameObject
{
public:
	World(rp3d::DynamicsWorld* physicsWorld);
};

class WorldDrawComponent : public DrawComponent
{
private:
	ObjModel * model;
public:
	void update(float elapsedSeconds) {}
};
