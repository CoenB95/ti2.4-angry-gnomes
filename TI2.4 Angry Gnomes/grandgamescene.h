#pragma once

#include "gamestate.h"
#include "reactphysics3d.h"

using namespace rp3d;

class GrandGameScene : public GameDrawState
{
private:
	DynamicsWorld& physicsWorld;
	GameObject* world;
	GameObject* slingshot;
	GameObject* tee;
	vector<GameObject*> blocks;

public:
	GrandGameScene(DynamicsWorld& physicsWorld);
	void onInit() override;
	void onExit() override;
};
