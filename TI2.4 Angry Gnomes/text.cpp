#include <gl/freeglut.h>
#include <string>
#include <map>

#include "objectmodel.h"
#include "text.h"
#include "tinyxml/tinyxml2.h"

using namespace tinyxml2;

Text::Text(string text, string font, float scale, bool display3D) : textToPrint(text)
{
	if (display3D)
		addComponent(new Text3DDrawComponent(&textToPrint, 
			scale < 0.0f ? new BitmapTextLoader() : new BitmapTextLoader(font, scale)));
	else
		addComponent(new TextDrawComponent(&textToPrint));
}

const string BitmapTextLoader::FONT_BLOCKY = "text/minecraft_font.fnt";
BitmapTextLoader* BitmapTextLoader::defaultFont = nullptr;

BitmapTextLoader::BitmapTextLoader(string fontFilename, float scale) :
	font(fontFilename),
	scale(scale)
{
	loadBitmapFont(fontFilename);
}

BitmapTextLoader::~BitmapTextLoader()
{
	//delete bitmap;
}

void BitmapTextLoader::drawText(string text)
{
	if (!bitmapLoaded)
		return;

	bitmap->apply();
	glListBase(bitmapListStart);
	glCallLists(text.length(), GL_UNSIGNED_BYTE, text.c_str());
}

void BitmapTextLoader::loadBitmapFont(string filename)
{
	parseFontInfoFile(filename);

	bitmap = Texture::open(charInfoMap.pages[0]);
	bitmap->apply();

	bitmapListStart = glGenLists(charInfoMap.charInfos.size());

	for (int index = 0; index < 256; index++)
	{
		if (charInfoMap.charInfos.find(index) == charInfoMap.charInfos.end())
			continue;

		float characterX = charInfoMap.charInfos[index].x;
		float characterY = charInfoMap.charInfos[index].y;
		float characterW = charInfoMap.charInfos[index].w;
		float characterH = charInfoMap.charInfos[index].h;
		float hw = characterW * scale / 2.0f;
		float h = characterH * scale;

		glNewList(bitmapListStart + index, GL_COMPILE);
		glTranslatef(hw, -charInfoMap.charInfos[index].offsetY * scale, 0.0f);
		glBegin(GL_TRIANGLES);

		glTexCoord2f(characterX, characterY); //Top-left
		glVertex2f(-hw, 0.0f);
		glTexCoord2f(characterX, characterY + characterH); //Bottom-left
		glVertex2f(-hw, -h);
		glTexCoord2f(characterX + characterW, characterY); //Top-right
		glVertex2f(hw, 0.0f);

		glTexCoord2f(characterX + characterW, characterY); //Top-right
		glVertex2f(hw, 0.0f);
		glTexCoord2f(characterX, characterY + characterH); //Bottom-left
		glVertex2f(-hw, -h);
		glTexCoord2f(characterX + characterW, characterY + characterH); //Bottom-right
		glVertex2f(hw, -h);

		glEnd();
		glTranslatef(hw + 2.0f / 256.0f * scale, charInfoMap.charInfos[index].offsetY * scale, 0.0f);
		glEndList();
	}

	bitmapLoaded = true;
}

void BitmapTextLoader::parseFontInfoFile(const string filename)
{
	charInfoMap = {};

	XMLDocument doc;
	doc.LoadFile(filename.c_str());
	if (doc.Error())
	{
		cout << "Error passing font-file: " << doc.ErrorStr() << endl;
	}
	else
	{
		XMLElement* fontElement = doc.FirstChildElement("font");
		if (fontElement != nullptr)
		{
			XMLElement* pagesElement = fontElement->FirstChildElement("pages");
			if (pagesElement != nullptr)
			{
				XMLElement* pageEl = pagesElement->FirstChildElement("page");
				do
				{
					int id = pageEl->IntAttribute("id");
					string file = pageEl->Attribute("file");

					charInfoMap.pages[id] = file;

					pageEl = pageEl->NextSiblingElement("page");
				} while (pageEl != nullptr);
			}

			XMLElement* charsElement = fontElement->FirstChildElement("chars");
			if (charsElement != nullptr)
			{
				XMLElement* charEl = charsElement->FirstChildElement("char");
				if (charEl != nullptr)
				{
					do
					{
						int id = charEl->IntAttribute("id");
						float x = charEl->IntAttribute("x") / 256.0f;
						float y = charEl->IntAttribute("y") / 256.0f;
						float w = charEl->IntAttribute("width") / 256.0f;
						float h = charEl->IntAttribute("height") / 256.0f;
						float offsetX = charEl->IntAttribute("xoffset") / 256.0f;
						float offsetY = charEl->IntAttribute("yoffset") / 256.0f;

						CharInfo info = { x, y, w, h, offsetX, offsetY };
						charInfoMap.charInfos[id] = info;

						charEl = charEl->NextSiblingElement("char");
					} while (charEl != nullptr);
				}
			}
			else
			{
				cout << "Could not locate expected <chars> element.";
			}
		}
		else
		{
			cout << "Could not locate expected <font> element.";
		}
	}

	cout << "Done." << endl;
}

float BitmapTextLoader::textHeight(string text)
{
	float maxHeight = 0.0f;
	for (char c : text)
	{
		if (charInfoMap.charInfos[c].h > maxHeight)
			maxHeight = charInfoMap.charInfos[c].h;
	}
	return maxHeight * scale;
}

float BitmapTextLoader::textWidth(string text)
{
	float width = 0.0f;
	for (char c : text)
	{
		width += charInfoMap.charInfos[c].w;
		width += 2.0f / 256.0f;
	}
	width -= 2.0f / 256.0f;
	return width * scale;
}

/**
* Splits a string into substrings, based on a seperator
*/
vector<string> BitmapTextLoader::splitLine(string str, const string &seperator)
{
	std::vector<std::string> ret;
	size_t index;
	while (true)
	{
		index = str.find(seperator);
		if (index == std::string::npos)
			break;
		ret.push_back(str.substr(0, index));
		str = str.substr(index + 1);
	}
	ret.push_back(str);
	return ret;
}

TextDrawComponent::TextDrawComponent(string* text) : DrawComponent(), text(text) {
	
}

void TextDrawComponent::draw()
{
	glDisable(GL_TEXTURE_2D);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glRasterPos2f(0, 0);
	glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char *) text->c_str());
}

Text3DDrawComponent::Text3DDrawComponent(string* text, BitmapTextLoader* loader) : DrawComponent(),
text(text),
loader(loader)
{

}

Text3DDrawComponent::~Text3DDrawComponent()
{
	delete loader;
	loader = nullptr;
}

void Text3DDrawComponent::draw()
{
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glTranslatef(loader->textWidth(*text) * -0.5f, loader->textHeight(*text) * 0.5f, 0.0f);
	loader->drawText(*text);
}
