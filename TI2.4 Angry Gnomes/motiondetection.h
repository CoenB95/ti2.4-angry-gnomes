#ifndef MOTIONDETECTION_H
#define MOTIONDETECTION_H

#define POSTURE_PALM 2
#define POSTURE_FIST 1
#define POSTURE_NONE -1

#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp" 
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <stdio.h>
#include <string>
using namespace std;
using namespace cv;

struct DetectionResult {
	float deltaX;
	float deltaY;
	int handposture;
};

class MotionDetection
{
private:
	Mat binary16S;
	int iLowH = 80;
	int iHighH = 130;

	int iLowS = 200;
	int iHighS = 255;

	int iLowV = 100;
	int iHighV = 255;
	int centerLine;

	VideoCapture capture;
	Mat frame;
	float prevDeltaX;
	float prevDeltaY;

	int palmCount;

	String palm_cascadeName = "haarcascades/palm.xml";
	String fist_cascadeName = "haarcascades/fist.xml";
	CascadeClassifier fist_cascade, palm_cascade;

public:
	DetectionResult detectMotion(Mat& frame);
	VideoCapture* motionInit();
	void detectColorAndDraw(Mat& img,
		double scale);
	int detectAndDrawFist(Mat& img,
		CascadeClassifier& cascade,
		double scale);
	int detectAndDrawHand(Mat& img,
		CascadeClassifier& cascade,
		double scale);
	int calculateDistance(Mat &img);
	pair<float, float> calculateDelta(vector<Rect> objects, Mat& img);
	vector<Rect> detectPostures(Mat& img, CascadeClassifier& cascade, double scale);
	void drawPalmEllipses(Mat& img, double scale, vector<Rect> objects);
	void drawFistRectangle(Mat& img, double scale, vector<Rect> objects);
	void releaseResources();
};
#endif