#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES

#include <algorithm>
#include <cmath>
#include <iostream>
#include <gl/freeglut.h>

#include "gameobject.h"
#include "gameobjectcomponent.h"

using namespace std;

GameObject::GameObject()
{

}

GameObject::~GameObject()
{
	removeAllComponents();
}

void GameObject::addComponent(GameObjectComponent* component)
{
	component->setParent(this);

	DrawComponent* dc = dynamic_cast<DrawComponent*>(component);
	if (dc != nullptr)
	{
		if (drawComponent != nullptr)
			cout << "Warning: multiple draw-components supplied." << endl;

		drawComponent = dc;
	}

	this->components.push_back(component);
}

void GameObject::draw()
{
	if (drawComponent != nullptr)
	{
		glPushMatrix();
		
		glTranslatef(position.x, position.y, position.z);
		
		float rotationAngleRad;
		Vector3 rotationAxis;
		rotation.getRotationAngleAxis(rotationAngleRad, rotationAxis);

		glRotatef(rotationAngleRad / M_PI * 180.0f, rotationAxis.x, rotationAxis.y, rotationAxis.z);

		drawComponent->draw();

		glPopMatrix();
	}
}

void GameObject::removeAllComponents()
{
	for (GameObjectComponent* component : components)
	{
		if (component != nullptr)
			delete component;
	}

	components.clear();
}

void GameObject::removeComponent(GameObjectComponent* component)
{
	if (component != nullptr)
	{
		components.erase(std::remove(components.begin(), components.end(), component), components.end());
		delete component;
	}
}

void GameObject::update(float elapsedSeconds)
{
	for (GameObjectComponent* component : components)
	{
		component->update(elapsedSeconds);
	}
}

void GameObject::removeDrawComponent() {
	drawComponent = nullptr;
}