#pragma once
#pragma warning(disable:4996)

#include <mutex>

#include "ball.h"
#include "gamestate.h"
#include "motiondetection.h"

class AimGameState : public GameState
{
private:
	MotionDetection m;
	thread thrd;
	float rotationX;
	float rotationY;
	bool shouldLaunch;
	bool stopMotionDetection;
	float targetRotateX;
	float targetRotateY;

public:
	static Mat cameraImage;
	static mutex cameraImageLock;

	AimGameState(Ball* ball);
	~AimGameState();
	void onExit() override;
	void onInit() override;
	void onKeyCodePressed(int key) override;
	void onKeyPressed(unsigned char key) override;
	void onUpdate(float elapsedSeconds) override;
};
