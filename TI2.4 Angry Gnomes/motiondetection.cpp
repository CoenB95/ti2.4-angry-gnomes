#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp" 
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>
#include <stdio.h>
#include "motiondetection.h"

using namespace cv;
using namespace std;

VideoCapture* MotionDetection::motionInit() {
	this->capture = VideoCapture(0);
	fist_cascade = CascadeClassifier(fist_cascadeName);
	palm_cascade = CascadeClassifier(palm_cascadeName);
	capture.set(CV_CAP_PROP_FOURCC, CV_FOURCC('M', 'J', 'P', 'G'));
	capture.set(CV_CAP_PROP_BUFFERSIZE, 3);
	if (!capture.isOpened())
		return nullptr;
	CvSize size = cvSize((int)capture.get(CV_CAP_PROP_FRAME_WIDTH), (int)capture.get(CV_CAP_PROP_FRAME_HEIGHT));
	return &capture;
}

void MotionDetection::detectColorAndDraw(Mat& img, double scale) {
	Mat labeledImage;
	Mat hsvImage;
	cvtColor(img, hsvImage, COLOR_BGR2HSV);
	Mat imgThresholded;
	inRange(hsvImage, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded);

	erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(10, 10)));
	dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

	//morphological closing (fill small holes in the foreground)
	dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(10, 10)));
	erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
}

int MotionDetection::calculateDistance(Mat &img) {
	Moments mu = moments(img, true);
	Point center;
	center.x = (int)(mu.m10 / mu.m00);
	center.y = (int)(mu.m01 / mu.m00);
	circle(img, center, 2, Scalar(0, 0, 255));
	cout << "Afstand: " << centerLine - center.x << endl;
	return centerLine - center.x;
}

pair<float, float> MotionDetection::calculateDelta(vector<Rect> objects, Mat& img) {
	Point center;
	vector<Rect>::const_iterator r = objects.begin();
	center.x = cvRound((r->x + r->width*0.5)*1);
	center.y = cvRound((r->y + r->height*0.5)*1);
	float deltaX = (float)(((img.size().width / 2) - center.x)*-1);
	float deltaY = (float)((img.size().height / 2) - center.y);
	cout << "delta x: " << deltaX << endl;
	cout << "delta y: " << deltaY << endl;
	return make_pair(deltaX, deltaY);
}

DetectionResult MotionDetection::detectMotion(Mat& frame) {
	//capture >> frame; // get a new frame from camera
					  //cvtColor(frame, edges, CV_BGR2GRAY);
	DetectionResult result = { 0,0,0 };
	if (!capture.isOpened())
		return result;
	vector<Rect> fists = detectPostures(frame, fist_cascade, 1);
	vector<Rect> palms = detectPostures(frame, palm_cascade, 1);
	if (fists.size() >= 1)
	{
		// It's a fist!
		palmCount = 0;
		result.deltaX = calculateDelta(fists, frame).first;
		result.deltaY = calculateDelta(fists, frame).second;
		result.handposture = 1;

		drawFistRectangle(frame, 1, fists);
	}
	else if (palms.size() >= 1)
	{
		// It's a palm!
		result.deltaX = calculateDelta(palms, frame).first;
		result.deltaY = calculateDelta(palms, frame).second;
		palmCount++;
		
		//cout << "Palm count: " << palmCount << endl;	
		if (palmCount >= 10) {
			palmCount = 0;
			result.handposture = 2;
		}
		else {
			result.handposture = 1;
		}
		drawPalmEllipses(frame, 1, palms);
		
	}
	else
	{
		// Nothing detected
		result.handposture = POSTURE_NONE;
	}

	if (result.handposture != POSTURE_NONE)
	{
		float threshold = 40;
		if (abs(result.deltaX - prevDeltaX) > threshold)
			result.deltaX = prevDeltaX;
		if (abs(result.deltaY - prevDeltaY) > threshold)
			result.deltaY = prevDeltaY;

		prevDeltaX = result.deltaX;
		prevDeltaY = result.deltaY;
	}

	//imshow("result", frame);
	return result;
}

vector<Rect> MotionDetection:: detectPostures(Mat& img, CascadeClassifier& cascade, double scale) {
	vector<Rect> objects;
	Mat gray, smallImg(cvRound(img.rows / scale), cvRound(img.cols / scale), CV_8UC1);
	cvtColor(img, gray, CV_BGR2GRAY);
	resize(gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR);
	equalizeHist(smallImg, smallImg);
	cascade.detectMultiScale(smallImg, objects, 1.1, 2, 0
		| CV_HAAR_FIND_BIGGEST_OBJECT
		| CV_HAAR_SCALE_IMAGE,
		Size(50, 50));
	return objects;	
}

void MotionDetection::drawFistRectangle(Mat& img, double scale, vector<Rect> objects) {
	int i = 0;
	const static Scalar colors[] = { CV_RGB(0,0,255),
		CV_RGB(0,128,255),
		CV_RGB(0,255,255),
		CV_RGB(0,255,0),
		CV_RGB(255,128,0),
		CV_RGB(255,255,0),
		CV_RGB(255,0,0),
		CV_RGB(255,0,255) };
	for (vector<Rect>::const_iterator r = objects.begin(); r != objects.end(); r++, i++)
	{
		Point center;
		Scalar color = colors[i % 8];
		int radius;
		center.x = cvRound((r->x + r->width*0.5)*scale);
		center.y = cvRound((r->y + r->height*0.5)*scale);
		radius = cvRound((r->width + r->height)*0.25*scale);
		rectangle(img, Point(center.x + radius, center.y + radius), Point(center.x - radius, center.y - radius), color);
	}
}

void MotionDetection::drawPalmEllipses(Mat& img, double scale, vector<Rect> objects) {
	int i = 0;
	const static Scalar colors[] = { CV_RGB(0,0,255),
		CV_RGB(0,128,255),
		CV_RGB(0,255,255),
		CV_RGB(0,255,0),
		CV_RGB(255,128,0),
		CV_RGB(255,255,0),
		CV_RGB(255,0,0),
		CV_RGB(255,0,255) };
	for (vector<Rect>::const_iterator r = objects.begin(); r != objects.end(); r++, i++)
	{
		Point center;
		Scalar color = colors[i % 8];
		int radius;
		center.x = cvRound((r->x + r->width*0.5)*scale);
		center.y = cvRound((r->y + r->height*0.5)*scale);
		radius = cvRound((r->width + r->height)*0.25*scale);
		//circle(img, center, radius, color, 2, 8, 0);
		RotatedRect rect(center, Size(r->width, r->height), 0.0f);
		ellipse(img, rect, color);
	}
}

void MotionDetection::releaseResources()
{
	capture.release();
}
