#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES

#include <GL/freeglut.h>
#include <cmath>
#include <iostream>

#include "camera.h"

void Camera::applyTransform()
{
	// NOTE: The real z-axis is inverted. So apply it here.

	//float rotRadX = (rotateX) / 180.0f * M_PI;
	//float rotRadY = (rotateY - 90.0f) / 180.0f * M_PI;

	float rotationAngle;
	float rotationAngleRad;
	Vector3 rotationAxis;
	rotation.getRotationAngleAxis(rotationAngleRad, rotationAxis);
	rotationAngle = rotationAngleRad / M_PI * 180.0f;
	//cout << "Camera angle=" << rotationAngle << " deg, axis=" <<
	//	rotationAxis.x << ' ' << rotationAxis.y << ' ' << rotationAxis.z << endl;

	switch (type)
	{
	case CAMERA_TYPE_FIRST_PERSON:
		// Look from ourselves, to a point slightly in front.
		glRotatef(rotationAngle, rotationAxis.x, rotationAxis.y, rotationAxis.z);
		//glRotatef(rotateX, 1, 0, 0);
		//glRotatef(rotateY, 0, 1, 0);
		glTranslatef(-position.x, -position.y, -position.z);
		break;
	case CAMERA_TYPE_THIRD_PERSON:
		glTranslatef(0, 0, -thirdPersonDistance);
		glRotatef(rotationAngle, rotationAxis.x, rotationAxis.y, rotationAxis.z);
		glTranslatef(-position.x, -position.y, -position.z);
		// Look from a point slightly behind, to ourselves.
		//gluLookAt(
			//position.x - cosf(rotRadX) * cosf(rotRadY) * thirdPersonDistance,
			//position.y + sinf(rotRadX) * thirdPersonDistance,
			//position.z - cosf(rotRadX) * -sinf(rotRadY) * thirdPersonDistance,
			//rotationAxes.x * thirdPersonDistance,
			//position.x, position.y, position.z, 0, 1, 0);
		break;
	case CAMERA_TYPE_THIRD_PERSON_FRONT:
		glTranslatef(-position.x, -position.y, -position.z - thirdPersonDistance);
		glRotatef(rotationAngle, rotationAxis.x, rotationAxis.y, rotationAxis.z);
		// Look from a point slightly in front, to ourselves.
		//gluLookAt(
			//position.x + cosf(rotRadX) * cosf(rotRadY) * thirdPersonDistance,
			//position.y - sinf(rotRadX) * thirdPersonDistance,
			//position.z + cosf(rotRadX) * -sinf(rotRadY) * thirdPersonDistance,
			//position.x, position.y, position.z, 0, 1, 0);
		break;
	}
}

void Camera::update(float elapsedSeconds)
{
	GameObject::update(elapsedSeconds);
	thirdPersonDistance = thirdPersonDistanceSnappyness * thirdPersonDistance +
		(1.0f - thirdPersonDistanceSnappyness) * thirdPersonTargetDistance;
}
