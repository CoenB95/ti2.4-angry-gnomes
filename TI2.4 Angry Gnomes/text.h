#pragma once

#include <vector>

#include "gameobject.h"
#include "gameobjectcomponent.h"

typedef struct _CharInfo
{
	float x;
	float y;
	float w;
	float h;
	float offsetX;
	float offsetY;
} CharInfo;

typedef struct _BitmapFont
{
	map<int, string> pages;
	map<int, CharInfo> charInfos;
} BitmapFont;

class BitmapTextLoader
{
private:
	GLuint bitmapListStart = -1;
	bool bitmapLoaded = false;
	Texture* bitmap;
	BitmapFont charInfoMap;

	void parseFontInfoFile(const string filename);
	vector<string> splitLine(string str, const string &seperator);

public:
	static BitmapTextLoader* defaultFont;
	static const string FONT_BLOCKY;

	string font;
	float scale;

	BitmapTextLoader(string fontFilename = FONT_BLOCKY, float scale = 16.0f);
	~BitmapTextLoader();
	void drawText(string text);
	void loadBitmapFont(string filename);
	float textHeight(string text);
	float textWidth(string text);
};

class Text : public GameObject
{
private:
	string textToPrint;

public:
	Text(string text, string font = BitmapTextLoader::FONT_BLOCKY, float scale = -1.0f, bool display3D = true);
	inline void setText(string text) { this->textToPrint = text; };
};

class TextDrawComponent : public DrawComponent {
private:
	string * text;

public:
	TextDrawComponent(string* text);
	void draw() override;

};

class Text3DDrawComponent : public DrawComponent {
private:
	string * text;
	BitmapTextLoader* loader;

public:
	Text3DDrawComponent(string* text, BitmapTextLoader* loader);
	~Text3DDrawComponent();
	void draw() override;
};
