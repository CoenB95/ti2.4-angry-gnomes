#include <string>

#include "modeldrawcomponent.h"
#include "physics.h"
#include "world.h"

using namespace std;

World::World(rp3d::DynamicsWorld* physicsWorld) : GameObject()
{
	addComponent(new ModelDrawComponent("models/world/world.obj"));
	addComponent(new BoxPhysicsComponent(physicsWorld, Vec3f(0.0f, -0.25f, 0.0f), Vec3f(0.0f, 0.0f, 0.0f), Vec3f(480.0f, 0.2f, 480.0f), 10000,true));
}
