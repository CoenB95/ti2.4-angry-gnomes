#pragma once

#include "gameobject.h"
#include "gameobjectcomponent.h"
#include "reactphysics3d.h"

class PhysicsComponent : public GameObjectComponent
{
private:
	rp3d::CollisionShape* shape;
	rp3d::DynamicsWorld* world;

protected:
	rp3d::RigidBody* body = nullptr;
	void setup(rp3d::DynamicsWorld* world, rp3d::Transform initialTransform,rp3d::CollisionShape* shape,
		rp3d::Transform shapeTransform, float mass, bool isStatic);

public:
	PhysicsComponent();
	virtual ~PhysicsComponent();
	void applyForce(vec::Vec3f value);
	inline bool isSleeping() { return body->isSleeping(); };
	void update(float elapsedSeconds) override;
	void unregister();
};

class BallPhysicsComponent : public PhysicsComponent
{
public:
	BallPhysicsComponent(reactphysics3d::DynamicsWorld* world, vec::Vec3f position,
		float radius, bool isStatic = false);
	~BallPhysicsComponent() {};
};

class BoxPhysicsComponent : public PhysicsComponent
{
public:
	BoxPhysicsComponent(reactphysics3d::DynamicsWorld* world, vec::Vec3f position, vec::Vec3f rotation,
		vec::Vec3f size, float mass, bool isStatic = false);
	~BoxPhysicsComponent() {};
};
