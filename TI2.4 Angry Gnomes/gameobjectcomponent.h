#pragma once

class GameObject;

class GameObjectComponent
{
protected:
	GameObject* parentObject;
public:
	GameObjectComponent();
	virtual ~GameObjectComponent() {};
	GameObjectComponent(GameObject* parent);
	inline void setParent(GameObject* object) { this->parentObject = object; }
	virtual void update(float elapsedSeconds) = 0;
};

class DrawComponent : public GameObjectComponent
{
public:
	DrawComponent();
	DrawComponent(GameObject* parent);
	virtual ~DrawComponent() {};
	virtual void draw() = 0;
	virtual void update(float elapsedSeconds) {};
};

class SpinComponent : public GameObjectComponent
{
private:
	float degreesPerSecX;
	float degreesPerSecY;
	float degreesPerSecZ;
	float valueX;
	float valueY;
	float valueZ;
public:
	SpinComponent(float degreesPerSecX, float degreesPerSecY, float degreesPerSecZ,
		float valueX = 0.0f, float valueY = 0.0f, float valueZ = 0.0f);
	void update(float elapsedSeconds) override;
};
