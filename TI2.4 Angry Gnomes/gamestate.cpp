#include <vector>

#include "camera.h"
#include "gameobject.h"
#include "gamestate.h"

GameState::GameState()
{
	
}

bool GameState::isKeyPressed(unsigned char key)
{
	return manager->isKeyPressed(key);
};

void GameState::onExit()
{

}

void GameState::onInit()
{

}

void GameState::onKeyCodePressed(int key)
{
	
}

void GameState::onKeyPressed(unsigned char key)
{
	manager->pressedKeys[key] = true;
}

void GameState::onKeyCodeReleased(int key)
{
	
}

void GameState::onKeyReleased(unsigned char key)
{
	manager->pressedKeys[key] = false;
}

void GameState::onMouseMoved(bool activeMotion, int x, int y)
{

}

void GameState::onUpdate(float elapsedSeconds)
{
	for (GameObject* object : manager->gameObjects3D)
		object->update(elapsedSeconds);

	for (GameObject* object : manager->gameObjects2D)
		object->update(elapsedSeconds);

	manager->getCamera()->update(elapsedSeconds);
}

// GameDrawState

GameDrawState::GameDrawState() : GameState()
{

}

// Manager

GameStateManager::GameStateManager()
{
	manager = this;
	memset(pressedKeys, 0, sizeof(pressedKeys));
}

GameStateManager::~GameStateManager()
{
	manager = nullptr;
}

GameStateManager* GameStateManager::manager = nullptr;

void GameStateManager::changeState(GameState* newState)
{
	cout << "Queue State Change" << endl;
	queuedStates.push_back(newState);
}

void GameStateManager::checkStateChange()
{
	for (GameState* newState : queuedStates)
	{
		GameState* stateToUpdate = nullptr;
		GameDrawState* gds = dynamic_cast<GameDrawState*>(newState);

		if (newState == nullptr || gds == nullptr)
		{
			// The state defines a new way of updating.
			stateToUpdate = state;
			if (state != nullptr)
			{
				state->onExit();
				//state->manager = nullptr;
				delete state;
				state = nullptr;
			}

			camera.removeAllComponents();

			state = newState;

			if (state != nullptr)
			{
				cout << "Changed state" << endl;
				state->manager = this;
				state->onInit();
			}
			else
				cout << "Cleared state" << endl;
		}

		if (newState == nullptr || gds != nullptr)
		{
			// The state defines a new way of drawing.
			if (drawState != nullptr)
			{
				drawState->onExit();
				delete drawState;
				drawState = nullptr;
			}

			drawState = gds;

			if (drawState != nullptr)
			{
				cout << "Changed draw-state" << endl;
				drawState->manager = this;
				drawState->onInit();
			}
			else
				cout << "Cleared draw-state" << endl;
		}
	}

	queuedStates.clear();
}

void GameStateManager::draw2D()
{
	for (GameObject* object : gameObjects2D)
		object->draw();

	if (drawState != nullptr)
		drawState->onDraw2D();
}

void GameStateManager::draw3D()
{
	getCamera()->applyTransform();

	for (GameObject* object : gameObjects3D)
		object->draw();

	if (drawState != nullptr)
		drawState->onDraw3D();
}

GameStateManager* GameStateManager::getInstance()
{
	return manager;
}

void GameStateManager::removeAndDelete2DObject(GameObject* object)
{
	vector<GameObject*>::iterator it = find(gameObjects2D.begin(), gameObjects2D.end(), object);
	if (it == gameObjects2D.end())
	{
		cout << "Warning: Could not remove 2D object." << endl;
		return;
	}

	delete object;
	gameObjects2D.erase(it);
}

void GameStateManager::removeAndDelete3DObject(GameObject* object)
{
	vector<GameObject*>::iterator it = find(gameObjects3D.begin(), gameObjects3D.end(), object);
	if (it == gameObjects3D.end())
	{
		cout << "Warning: Could not remove 3D object." << endl;
		return;
	}

	delete object;
	gameObjects3D.erase(it);
}

void GameStateManager::updateState(float elapsedSeconds)
{
	getInstance()->checkStateChange();

	if (hasState())
	{
		getInstance()->getCurrentState()->onUpdate(elapsedSeconds);
	}
}
