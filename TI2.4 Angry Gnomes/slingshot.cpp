#include <string>

#include "modeldrawcomponent.h"
#include "slingshot.h"

using namespace std;

Slingshot::Slingshot() : GameObject()
{
	addComponent(new ModelDrawComponent("models/catapult/slingshot.obj"));
}
