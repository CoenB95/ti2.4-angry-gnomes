#pragma once

#include "gameobject.h"
#include "gameobjectcomponent.h"
#include "objectmodel.h"

class Slingshot : public GameObject
{
public:
	Slingshot();
};

class SlingshotDrawComponent : public DrawComponent
{
private:
	ObjModel * model;
public:
	void update(float elapsedSeconds) {}
};
