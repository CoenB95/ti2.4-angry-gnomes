#pragma once

#include "gamestate.h"
#include "gameobject.h"
#include "text.h"

class FollowBallGameState : public GameState
{
private:
	GameObject* ball;
	Text* debugText;
	float distance;
	bool shouldSpin;
	float snappyness;

	float timeBallOutOfWorld = 0.0f;

public:
	FollowBallGameState(GameObject* ball, float snappyness = 0.0f, float distance = 16.0f, bool spin = true);
	void onExit() override;
	void onInit() override;
	void onKeyCodePressed(int key) override;
	void onKeyPressed(unsigned char key) override;
	void onUpdate(float elapsedSeconds) override;
};
