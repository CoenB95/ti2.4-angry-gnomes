﻿#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES

#include <reactphysics3d.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <GL/freeglut.h>
#include <vector>
#include <cmath>

#include "ball.h"
#include "block.h"
#include "camera.h"
#include "followballgamestate.h"
#include "followcomponent.h"
#include "gameobject.h"
#include "gamestate.h"
#include "grandgamescene.h"
#include "main.h"
#include "menustate.h"
#include "motiondetection.h"
#include "objectmodel.h"
#include "slingshot.h"
#include "text.h"
#include "world.h"

using namespace std;

GameStateManager stateManager;

rp3d::Vector3 gravity(0.0f, -9.81f * 3, 0.0f);
rp3d::DynamicsWorld physicsWorld(gravity);

float lastFrameTime = 0;
float lastWorldUpdateTime = 0;

// We don't care for a profiler. This definition does nothing.
void b3BeginProfileScope(const char* name)
{

}

// We don't care for a profiler. This definition does nothing.
void b3EndProfileScope()
{

}

int main(int argc, char* argv[])
{
	CV_WINDOW_FULLSCREEN;
	glutInit(&argc, argv);
	glutInitWindowSize(1024, 768);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutCreateWindow("GLUT example");
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Register callbacks:
	glutDisplayFunc(onDraw);
	glutReshapeFunc(onSizeChanged);
	glutSpecialFunc(onKeyCodePressed);
	glutIdleFunc(onUpdate);
	glutKeyboardFunc(onKeyPressed);
	glutKeyboardUpFunc(onKeyReleased);
	glutPassiveMotionFunc(onMouseMovedPassive);

	physicsWorld.setSleepLinearVelocity(2.5f);
	physicsWorld.setSleepAngularVelocity(1.0f);

	BitmapTextLoader::defaultFont = new BitmapTextLoader();

	GameStateManager::getInstance()->changeState(new MenuState(physicsWorld));

	// Turn the flow of control over to GLUT
	glutMainLoop();

    return 0;
}

void onDraw()
{
	glViewport(0, 0, stateManager.width, stateManager.height);
	glClearColor(0.6f, 0.6f, 0.9f, 1.0f);

	// 3D drawings
	glMatrixMode(GL_PROJECTION);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	gluPerspective(70, (GLdouble)stateManager.width / stateManager.height, 0.1f, 1000);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GameStateManager::getInstance()->draw3D();

	// 2D drawings
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, stateManager.width, stateManager.height, 0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GameStateManager::getInstance()->draw2D();

	glutSwapBuffers();
}

void onKeyCodePressed(int keyCode, int mouseX, int mouseY)
{
	if (GameStateManager::hasState())
		GameStateManager::getInstance()->getCurrentState()->onKeyCodePressed(keyCode);
}

void onKeyPressed(unsigned char key, int, int)
{
	if (key == 27)
	{
		// Cleanup
		stateManager.changeState(nullptr);
		delete BitmapTextLoader::defaultFont;

		//glutLeaveMainLoop();
		exit(0);
	}	
	if (key == 114) {
		// Clear any draw and update states.
		GameStateManager::getInstance()->changeState(nullptr);
		GameStateManager::getInstance()->changeState(new MenuState(physicsWorld));
	}

	if (GameStateManager::hasState())
		GameStateManager::getInstance()->getCurrentState()->onKeyPressed(key);
}

void onSizeChanged(GLint w, GLint h)
{
	stateManager.width = w;
	stateManager.height = h;
}

void onUpdate()
{
	float frameTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;
	float elapsedTime = frameTime - lastFrameTime;
	lastFrameTime = frameTime;
	
	// Perform a time step of the world in this frame.
	if (lastWorldUpdateTime <= 0)
		lastWorldUpdateTime = lastFrameTime;

	while (lastFrameTime > lastWorldUpdateTime + WORLD_UPDATE_INTERVAL)
	{
		lastWorldUpdateTime += WORLD_UPDATE_INTERVAL;
		physicsWorld.update(WORLD_UPDATE_INTERVAL);
	}

	GameStateManager::updateState(elapsedTime);

	glutPostRedisplay();
}

void onMouseMovedPassive(int x, int y)
{
	if (GameStateManager::hasState())
		GameStateManager::getInstance()->getCurrentState()->onMouseMoved(false, x, y);
}

void onKeyReleased(unsigned char key, int, int)
{
	if (GameStateManager::hasState())
		GameStateManager::getInstance()->getCurrentState()->onKeyReleased(key);
}
