#include <string>

#include "modeldrawcomponent.h"
#include "block.h"
#include "physics.h"
#include "reactphysics3d.h"

using namespace std;

Block::Block(rp3d::DynamicsWorld* physicsBlock, string type, vec::Vec3f position, vec::Vec3f rotation, vec::Vec3f size, float mass, bool isStatic) : GameObject()
{
	float sizeX, sizeY, sizeZ;
	string filename;

	if (type == "TYPE_BEAM")
	{
		sizeX = BEAM_SIZE_X;
		sizeY = BEAM_SIZE_Y;
		sizeZ = BEAM_SIZE_Z;
		filename = TYPE_BEAM;
	}
	else if (type == "TYPE_POLE")
	{
		sizeX = POLE_SIZE_X;
		sizeY = POLE_SIZE_Y;
		sizeZ = POLE_SIZE_Z;
		filename = TYPE_POLE;
	}
	else if (type == "TYPE_PLANK")
	{
		sizeX = PLANK_SIZE_X;
		sizeY = PLANK_SIZE_Y;
		sizeZ = PLANK_SIZE_Z;
		filename = TYPE_PLANK;
	}
	else if (type == "TYPE_FLOOR")
	{
		sizeX = FLOOR_SIZE_X;
		sizeY = FLOOR_SIZE_Y;
		sizeZ = FLOOR_SIZE_Z;
		filename = TYPE_FLOOR;
	}

	addComponent(new ModelDrawComponent(filename));
	addComponent(new BoxPhysicsComponent(physicsBlock, position, rotation, Vec3f(sizeX, sizeY, sizeZ), mass, isStatic));
}
