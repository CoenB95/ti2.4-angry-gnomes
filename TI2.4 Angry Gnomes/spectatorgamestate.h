#pragma once

#include "gamestate.h"

class SpectatorGameState : public GameState
{
private:
	bool justMovedMouse = false;
	float rotateX;
	float rotateY;

public:
	static const float FLY_SPEED;
	SpectatorGameState();

	void onInit() override;
	void onKeyPressed(unsigned char key) override;
	void onMouseMoved(bool activeMotion, int x, int y);
	void onUpdate(float elapsedSeconds) override;

	void moveHorizontal(float angle, float fac, float elapsedSeconds);
	void moveVertical(float fac, float elapsedSeconds);
};
