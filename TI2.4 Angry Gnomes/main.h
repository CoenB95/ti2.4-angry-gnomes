#pragma once

#define FLY_SPEED 20.0f
#define WORLD_UPDATE_INTERVAL 0.005f

void onDraw(void);
void onKeyCodePressed(int keyCode, int mouseX, int mouseY);
void onKeyPressed(unsigned char key, int, int);
void onKeyReleased(unsigned char key, int, int);
void onMouseMovedPassive(int x, int y);
void onSizeChanged(GLint width, GLint height);
void onUpdate(void);

enum CameraState
{
	SPECTATOR, FOLLOW_BALL, PULL_CATAPULT
};
