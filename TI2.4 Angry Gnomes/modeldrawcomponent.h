#pragma once

#include "gameobject.h"
#include "gameobjectcomponent.h"
#include "objectmodel.h"
#include <map>

class ModelDrawComponent : public DrawComponent
{
private:
	static std::map<string, ObjModel*> cache;
	ObjModel* model;
public:
	ModelDrawComponent(const string fileName);
	void draw() override;
};
