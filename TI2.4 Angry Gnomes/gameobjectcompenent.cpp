#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES

#include <cmath>

#include "gameobject.h"
#include "gameobjectcomponent.h"

GameObjectComponent::GameObjectComponent()
{

}

GameObjectComponent::GameObjectComponent(GameObject* parent) : parentObject(parent)
{

}

DrawComponent::DrawComponent() : GameObjectComponent()
{

}

DrawComponent::DrawComponent(GameObject* parent) : GameObjectComponent(parent)
{

}

SpinComponent::SpinComponent(float degreesPerSecX, float degreesPerSecY, float degreesPerSecZ,
	float valueX, float valueY, float valueZ) : GameObjectComponent(),
degreesPerSecX(degreesPerSecX),
degreesPerSecY(degreesPerSecY),
degreesPerSecZ(degreesPerSecZ),
valueX(valueX),
valueY(valueY),
valueZ(valueZ)
{

}

void SpinComponent::update(float elapsedSeconds)
{
	valueX += degreesPerSecX * elapsedSeconds;
	valueY += degreesPerSecY * elapsedSeconds;
	valueZ += degreesPerSecZ * elapsedSeconds;
	while (valueX >= 360.0f)
		valueX -= 360.0f;
	while (valueY >= 360.0f)
		valueY -= 360.0f;
	while (valueZ >= 360.0f)
		valueZ -= 360.0f;

	parentObject->rotation =
		Quaternion::fromEulerAngles(0.0f, 0.0f, valueZ / 180.0f * M_PI) *
		Quaternion::fromEulerAngles(valueX / 180.0f * M_PI, 0.0f, 0.0f) *
		Quaternion::fromEulerAngles(0.0f, valueY / 180.0f * M_PI, 0.0f);
}
