#include <GL/freeglut.h>
#include <sstream>

#include "aimgamestate.h"
#include "followballgamestate.h"
#include "followcomponent.h"
#include "gameobjectcomponent.h"
#include "gamestate.h"
#include "text.h"
#include "vec.h"

using namespace std;

FollowBallGameState::FollowBallGameState(GameObject* ball, float snappyness, float distance, bool spin) : GameState(), 
ball(ball),
distance(distance),
shouldSpin(spin),
snappyness(snappyness)
{

}

void FollowBallGameState::onExit()
{
	manager->removeAndDelete2DObject(debugText);
}

void FollowBallGameState::onInit()
{
	manager->getCamera()->setType(Camera::CAMERA_TYPE_THIRD_PERSON);
	manager->getCamera()->thirdPersonTargetDistance = distance;
	manager->getCamera()->thirdPersonDistanceSnappyness = 0.99f;
	manager->getCamera()->addComponent(FollowComponent::rotatingAndTranslating(ball, snappyness));

	debugText = new Text("Test", BitmapTextLoader::FONT_BLOCKY, 12.0f, false);
	debugText->position = vec::Vec3f(10, 20, 0);
	manager->add2DObject(debugText);
}

void FollowBallGameState::onKeyCodePressed(int keyCode)
{
	GameState::onKeyCodePressed(keyCode);
	switch (keyCode)
	{
	case GLUT_KEY_F5:
		manager->getCamera()->toggleType();
		break;
	}
}

void FollowBallGameState::onKeyPressed(unsigned char key)
{
	GameState::onKeyPressed(key);
	if (key == 'f')
		manager->changeState(new AimGameState(manager->ball));
}

void FollowBallGameState::onUpdate(float elapsedSeconds)
{
	GameState::onUpdate(elapsedSeconds);

	stringstream strStream;
	vec::Vec3f worldCenter = vec::Vec3f();

	if (worldCenter.distance(manager->ball->position) > 500)
	{
		strStream << "Ball out of game" << endl;
		strStream << "Reset in " << to_string((int) (5 - timeBallOutOfWorld)) << endl;
		timeBallOutOfWorld += elapsedSeconds;
	}
	else
	{
		strStream << "Ball rolling" << endl;
		timeBallOutOfWorld = 0.0f;
	}

	if (timeBallOutOfWorld >= 5.0f)
	{
		manager->ball->reset();
		manager->changeState(new AimGameState(manager->ball));
	}

	debugText->setText(strStream.str());
}
