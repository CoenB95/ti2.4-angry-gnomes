#include <GL/freeglut.h>
#include <iostream>
#include <reactphysics3d.h>

#include "aimgamestate.h"
#include "gamestate.h"
#include "grandgamescene.h"
#include "menustate.h"
#include "text.h"
#include "vec.h"

MenuState::MenuState(rp3d::DynamicsWorld &physicsWorld) : GameState(),
physicsWorld(physicsWorld)
{

}


void MenuState::onInit()
{
	manager->getCamera()->setType(Camera::CAMERA_TYPE_THIRD_PERSON);
	// Create a nice zoom-in into the menu.
	manager->getCamera()->thirdPersonDistance = 100.0f;
	manager->getCamera()->thirdPersonTargetDistance = 16.0f;
	manager->getCamera()->thirdPersonDistanceSnappyness = 0.9f;
	manager->getCamera()->position = vec::Vec3f();
	manager->getCamera()->rotation = Quaternion::fromEulerAngles(0.0f, 0.0f, 0.0f);

	selectionRect = new SelectionRectangle(7.0f, 1.5f);
	titleText = new Text("Angry Gnomes", BitmapTextLoader::FONT_BLOCKY, 26.0f, true);
	startText = new Text("Start");
	settingsText = new Text("Settings");
	quitText = new Text("Quit");
	versionText = new Text("v1.0", BitmapTextLoader::FONT_BLOCKY, 8.0f, true);
	manager->add3DObject(selectionRect);
	manager->add3DObject(titleText);
	manager->add3DObject(startText);
	manager->add3DObject(settingsText);
	manager->add3DObject(quitText);
	manager->add3DObject(versionText);
	choice = 0;
}

void MenuState::selectUp()
{
	choice = (choice + 1) % 3;
}

void MenuState::selectDown()
{
	choice--;
	if (choice < 0) {
		choice = 2;
	}
}

void MenuState::enterScreen(int choise) {
	std::cout << " Entering screen: " << choice << endl;
	switch (choice)
	{
	case Start:
		manager->changeState(new GrandGameScene(physicsWorld));
		manager->changeState(new AimGameState(manager->ball));
		break;
	case Settings:

		break;
	case Quit:
		exit(0);
		break;
	default:
		break;
	}
}

void MenuState::exitSettings()
{

}

void MenuState::onKeyPressed(unsigned char key)
{
	GameState::onKeyPressed(key);
	switch (key)
	{
	case '\r':
	case '\n':
		enterScreen(choice);
		break;
	}
}

void MenuState::onKeyCodePressed(int keyCode)
{
	GameState::onKeyCodePressed(keyCode);
	switch (keyCode)
	{
	case GLUT_KEY_UP:
		selectDown();
		break;
	case GLUT_KEY_DOWN:
		selectUp();
		break;
	}
}

void MenuState::onExit() {
	manager->removeAndDelete3DObject(selectionRect);
	manager->removeAndDelete3DObject(titleText);
	manager->removeAndDelete3DObject(startText);
	manager->removeAndDelete3DObject(settingsText);
	manager->removeAndDelete3DObject(quitText);
	manager->removeAndDelete3DObject(versionText);
}

void MenuState::onUpdate(float elapsedSeconds)
{
	GameState::onUpdate(elapsedSeconds);
	int itemCount = 3;
	float top = (float)itemCount / 2.0f * itemSpacing;
	titleText->position = vec::Vec3f(0.0f, top + -2 * -itemSpacing, 0.0f);
	startText->position = vec::Vec3f(0.0f, top + 0 * -itemSpacing, 0.0f);
	settingsText->position = vec::Vec3f(0.0f, top + 1 * -itemSpacing, 0.0f);
	quitText->position = vec::Vec3f(0.0f, top + 2 * -itemSpacing, 0.0f);
	selectionRect->position = vec::Vec3f(0.0f, top + choice * -itemSpacing, 0.0f);
	versionText->position = vec::Vec3f(0.0f, top + 3 * -itemSpacing, 0.0f);
}

SelectionRectangle::SelectionRectangle(float width, float height) :
	width(width), height(height)
{
	//addComponent(new RectangleFillDrawComponent());
	addComponent(new RectangleOutlineDrawComponent());
}

void RectangleFillDrawComponent::draw()
{
	SelectionRectangle* rect = dynamic_cast<SelectionRectangle*>(parentObject);
	if (rect == nullptr)
		return;

	float hw = rect->width / 2.0f;
	float hh = rect->height / 2.0f;

	glDisable(GL_TEXTURE_2D);
	glBegin(GL_TRIANGLES);
	glColor3f(1, 1, 0);

	glVertex2i(hw, hh);
	glVertex2i(-hw, hh);
	glVertex2i(-hw, -hh);

	glVertex2i(hw, hh);
	glVertex2i(-hw, -hh);
	glVertex2i(hw, -hh);

	glEnd();
}

void RectangleOutlineDrawComponent::draw()
{
	SelectionRectangle* rect = dynamic_cast<SelectionRectangle*>(parentObject);
	if (rect == nullptr)
		return;

	float hw = rect->width / 2.0f;
	float hh = rect->height / 2.0f;

	glDisable(GL_TEXTURE_2D);
	glLineWidth(5.0f);
	glBegin(GL_LINE_STRIP);
	glColor3f(1, 0, 1);

	glVertex2f(-hw, hh);
	glVertex2f(-hw, -hh);
	glVertex2f(hw, -hh);
	glVertex2f(hw, hh);
	glVertex2f(-hw, hh);

	glEnd();
}
