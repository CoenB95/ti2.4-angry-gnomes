#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES

#include <cmath>

#include "gameobject.h"
#include "gameobjectcomponent.h"
#include "physics.h"
#include "reactphysics3d.h"
#include "vec.h"

using namespace reactphysics3d;

PhysicsComponent::PhysicsComponent() : GameObjectComponent()
{
	
}

PhysicsComponent::~PhysicsComponent()
{
	unregister();
}

void PhysicsComponent::setup(rp3d::DynamicsWorld* world, rp3d::Transform initialTransform,rp3d::CollisionShape* shape,
	rp3d::Transform shapeTransform, float mass, bool isStatic)
{
	this->shape = shape;
	this->world = world;
	body = world->createRigidBody(initialTransform);
	body->addCollisionShape(shape, shapeTransform, mass);
	body->setType(isStatic ? rp3d::BodyType::STATIC : rp3d::BodyType::DYNAMIC);
}

void PhysicsComponent::applyForce(vec::Vec3f value) 
{
	body->applyForceToCenterOfMass(Vector3(value.x, value.y, value.z));
}

void PhysicsComponent::unregister()
{
	world->destroyRigidBody(body);
}

void PhysicsComponent::update(float elapsedSeconds)
{
	if (body == nullptr)
		return;

	Transform transform = body->getTransform();
	Vector3 position = transform.getPosition();
	Quaternion quat = transform.getOrientation();

	// roll (x-axis rotation)
	float roll;
	float sinr = +2.0f * (quat.w * quat.x + quat.y * quat.z);
	float cosr = +1.0f - 2.0f * (quat.x * quat.x + quat.y * quat.y);
	roll = atan2(sinr, cosr);

	// pitch (y-axis rotation)
	float pitch;
	float sinp = +2.0f * (quat.w * quat.y - quat.z * quat.x);
	if (fabs(sinp) >= 1)
		pitch = (float)copysign(M_PI / 2, sinp); // use 90 degrees if out of range
	else
		pitch = asin(sinp);

	// yaw (z-axis rotation)
	float yaw;
	float siny = +2.0f * (quat.w * quat.z + quat.x * quat.y);
	float cosy = +1.0f - 2.0f * (quat.y * quat.y + quat.z * quat.z);
	yaw = atan2(siny, cosy);

	//parentObject->rotateX = roll / M_PI * 180.0f;
	//parentObject->rotateY = pitch / M_PI * 180.0f;
	//parentObject->rotateZ = yaw / M_PI * 180.0f;
	float ang = 0.0f;
	Vector3 ax;
	quat.getRotationAngleAxis(ang, ax);
	//parentObject->rotationAxis = vec::Vec3f(ax.x, ax.y, ax.z);
	//parentObject->rotationAngle = ang / M_PI * 180.0f;
	parentObject->rotation = quat;
	parentObject->position = vec::Vec3f(position.x, position.y, position.z);
}

BallPhysicsComponent::BallPhysicsComponent(reactphysics3d::DynamicsWorld* world, vec::Vec3f position, float radius, bool isStatic)
{
	Vector3 initialPosition(position.x, position.y, position.z);
	Quaternion initialOrientation = rp3d::Quaternion::identity();
	Transform initialTransform(initialPosition, initialOrientation);
	setup(world, initialTransform, new SphereShape(radius), Transform::identity(), 1000.0f, isStatic);
}

BoxPhysicsComponent::BoxPhysicsComponent(reactphysics3d::DynamicsWorld* world, vec::Vec3f position, vec::Vec3f rotation, vec::Vec3f size, float mass,  bool isStatic)
{
	Vector3 initialPosition(position.x, position.y, position.z);
	rp3d::Quaternion initialOrientation = rp3d::Quaternion::fromEulerAngles(rotation.x * (float)M_PI / 180.0f, rotation.y * (float)M_PI / 180.0f, rotation.z * (float)M_PI / 180.0f);
	rp3d::Transform initialTransform(initialPosition, initialOrientation);
	Vector3 halfExtentSize(size.x / 2, size.y / 2, size.z / 2);
	setup(world, initialTransform, new BoxShape(halfExtentSize), Transform::identity(), mass, isStatic);
}
