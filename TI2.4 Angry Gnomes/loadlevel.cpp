#include <reactphysics3d.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <GL/freeglut.h>
#include <GL/GL.h>
#include <gl/glu.h>
#include <GL/glut.h>
#include <vector>
#include <cmath>

#include "ball.h"
#include<math.h>
#include "camera.h"
#include "block.h"
#include "gameobject.h"
#include "main.h"
#include "motiondetection.h"
#include "objectmodel.h"
#include "slingshot.h"
#include "world.h"
#include "loadlevel.h"

using namespace cv;
using namespace std;



int loadLevel(string filename, vector<GameObject*> &gameobjects, rp3d::DynamicsWorld *physicsWorld) {
	ifstream fin(filename.c_str());
	if (fin.fail()) {
		// throw error here
		cout << "Error: Could not open file." << endl;
		return 0;
	}
	int size;
	fin >> size;
	for (int count = 0; count < size; count++) {
		string type;
		float posX, posY, posZ;
		float rotX, rotY, rotZ;
		//float sizeX, sizeY, sizeZ;
		int mass;
		fin >> type >> posX >> posY >> posZ >> rotX >> rotY >> rotZ >> mass;
		GameObject* block = new Block(physicsWorld, type, vec::Vec3f(posX, posY, posZ), vec::Vec3f(rotX, rotY, rotZ), vec::Vec3f(), (float)mass, false);
		gameobjects.push_back(block);
	}
	return 0;
}

