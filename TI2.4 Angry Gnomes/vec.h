#pragma once

namespace vec
{
	class Vec3f
	{
	public:
		union
		{
			struct
			{
				float x, y, z;
			};
			float values[3];
		};
		Vec3f();
		Vec3f(float x, float y, float z);
		Vec3f(const Vec3f& other);
		float distance(Vec3f other);
		Vec3f operator +(Vec3f other);
		float& operator [](int);
	};

	class Vec2f
	{
	public:
		union
		{
			struct
			{
				float x, y;
			};
			float values[2];
		};
		Vec2f();
		Vec2f(float x, float y);
		Vec2f(const Vec2f& other);
		float& operator [](int);
	};
}
