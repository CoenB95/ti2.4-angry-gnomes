#pragma once

#include "ball.h"
#include "camera.h"
#include "gameobject.h"
#include "block.h"

class GameStateManager;

class GameState
{
public:
	GameStateManager* manager;

	GameState();

	bool isKeyPressed(unsigned char key);
	virtual void onExit();
	virtual void onInit();
	virtual void onKeyCodePressed(int key);
	virtual void onKeyPressed(unsigned char key);
	void onKeyCodeReleased(int key);
	void onKeyReleased(unsigned char key);
	virtual void onMouseMoved(bool activeMotion, int x, int y);
	virtual void onUpdate(float elapsedSeconds);
};

class GameDrawState : public GameState
{
public:
	GameDrawState();

	virtual void onDraw2D() {};
	virtual void onDraw3D() {};
};

class GameStateManager
{
private:
	static GameStateManager* manager;
	vector<GameState*> queuedStates;

	Camera camera;
	GameState* state = nullptr;
	GameDrawState* drawState = nullptr;

public:
	vector<GameObject*> gameObjects2D;
	vector<GameObject*> gameObjects3D;
	unsigned char pressedKeys[255];
	int width;
	int height;

	// Might be relocated later; now handy for states.
	Ball* ball;

	/// Reference to an (invisible) object representing the middle of the current level. Good for nice views.
	GameObject* levelCenter;

	GameStateManager();
	~GameStateManager();

	inline void add2DObject(GameObject* object) { gameObjects2D.push_back(object); };
	inline void add3DObject(GameObject* object) { gameObjects3D.push_back(object); };

	void changeState(GameState* newState);
	void checkStateChange();
	void draw2D();
	void draw3D();
	GameState* getCurrentState() { return state; };
	GameDrawState* getCurrentDrawState() { return drawState; };
	inline Camera* getCamera() { return &camera; };
	static GameStateManager* getInstance();
	inline static bool hasState() { return manager != nullptr && manager->state != nullptr; };
	inline bool isKeyPressed(unsigned char key) { return pressedKeys[key]; };
	void removeAndDelete2DObject(GameObject* object);
	void removeAndDelete3DObject(GameObject* object);
	static void updateState(float elapsedSeconds);
};
