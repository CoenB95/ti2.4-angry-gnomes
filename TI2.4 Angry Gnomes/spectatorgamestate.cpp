#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES

#include <GL/freeglut.h>
#include <cmath>

#include "followballgamestate.h"
#include "gameobjectcomponent.h"
#include "gamestate.h"
#include "spectatorgamestate.h"
#include "vec.h"

SpectatorGameState::SpectatorGameState() : GameState()
{

}

const float SpectatorGameState::FLY_SPEED = 20.0f;

void SpectatorGameState::onInit()
{
	manager->getCamera()->setType(Camera::CAMERA_TYPE_FIRST_PERSON);
	manager->getCamera()->position = vec::Vec3f(0, 30, 180);
}

void SpectatorGameState::onKeyPressed(unsigned char key)
{
	GameState::onKeyPressed(key);

	if (key == 'f')
	{
		manager->changeState(new FollowBallGameState(manager->ball));
	}
}

void SpectatorGameState::onMouseMoved(bool activeMotion, int x, int y)
{
	int dx = x - manager->width / 2;
	int dy = y - manager->height / 2;
	if ((dx != 0 || dy != 0) && abs(dx) < 400 && abs(dy) < 400 && !justMovedMouse)
	{
		//manager->getCamera()->rotateY += dx / 10.0f;
		//manager->getCamera()->rotateX += dy / 10.0f;
		rotateX += dy / 10.0f;
		rotateY += dx / 10.0f;
		manager->getCamera()->rotation =
			Quaternion::fromEulerAngles(rotateX / 180.0f * M_PI, 0.0f, 0.0f) *
			Quaternion::fromEulerAngles(0.0f, rotateY / 180.0f * M_PI, 0.0f);
	}
	if (!justMovedMouse)
	{
		glutWarpPointer(manager->width / 2, manager->height / 2);
		justMovedMouse = true;
	}
	else
		justMovedMouse = false;
}

void SpectatorGameState::onUpdate(float elapsedSeconds)
{
	GameState::onUpdate(elapsedSeconds);

	if (isKeyPressed('a')) moveHorizontal(0, FLY_SPEED, elapsedSeconds);
	if (isKeyPressed('d')) moveHorizontal(180, FLY_SPEED, elapsedSeconds);
	if (isKeyPressed('w')) moveHorizontal(90, FLY_SPEED, elapsedSeconds);
	if (isKeyPressed('s')) moveHorizontal(270, FLY_SPEED, elapsedSeconds);
	if (isKeyPressed(' ')) moveVertical(FLY_SPEED, elapsedSeconds);
	if (isKeyPressed('e')) moveVertical(-FLY_SPEED, elapsedSeconds);
}

void SpectatorGameState::moveHorizontal(float angle, float fac, float elapsedSeconds)
{
	manager->getCamera()->position.x -= (float)cosf((rotateY + angle) / 180 * M_PI)
		* fac * elapsedSeconds;
	manager->getCamera()->position.z -= (float)sinf((rotateY + angle) / 180 * M_PI)
		* fac * elapsedSeconds;
}

void SpectatorGameState::moveVertical(float fac, float elapsedSeconds)
{
	manager->getCamera()->position.y += fac * elapsedSeconds;
}
