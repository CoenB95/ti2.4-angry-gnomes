#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES

#include <cmath>
#include <vector>

#include "block.h"
#include "followcomponent.h"
#include "grandgamescene.h"
#include "loadlevel.h"
#include "slingshot.h"
#include "world.h"

using namespace rp3d;

GrandGameScene::GrandGameScene(DynamicsWorld& physicsWorld) : GameDrawState(),
physicsWorld(physicsWorld)
{
	world = new World(&physicsWorld);
	slingshot = new Slingshot();
	slingshot->position = Vec3f(0, 0, 180);
}

void GrandGameScene::onInit()
{
	manager->ball = new Ball(&physicsWorld, slingshot->position + Vec3f(0, 30, 0));
	tee = new Block(&physicsWorld, "TYPE_POLE", vec::Vec3f(0.0f, 14.0f, 180.0f), vec::Vec3f(90, 0, 0), vec::Vec3f(), (float)960, true);
	tee->removeDrawComponent();

	string filename = "levels/911level.txt";

	manager->add3DObject(world);
	manager->add3DObject(slingshot);
	manager->add3DObject(manager->ball);
	manager->add3DObject(tee);
	
	loadLevel(filename, blocks, &physicsWorld);
	for (GameObject* block : blocks)
		manager->add3DObject(block);

	manager->levelCenter = new GameObject();
	manager->levelCenter->addComponent(FollowComponent::translating(blocks[0], 0.0f));
	manager->levelCenter->addComponent(new SpinComponent(0.0f, 8.0f, 0.0f, 30.0f, 0.0f, 0.0f));

	manager->add3DObject(manager->levelCenter);
}

void GrandGameScene::onExit()
{
	manager->removeAndDelete3DObject(world);
	manager->removeAndDelete3DObject(slingshot);
	manager->removeAndDelete3DObject(manager->ball);
	manager->removeAndDelete3DObject(tee);
	manager->removeAndDelete3DObject(manager->levelCenter);

	for (GameObject* block : blocks)
		manager->removeAndDelete3DObject(block);

	manager->ball = nullptr;
	manager->levelCenter = nullptr;
	blocks.clear();
}
