#pragma once
#define TYPE_BEAM "models/beam/beam.obj"
#define TYPE_POLE "models/beam/table_wood_01.obj"
#define TYPE_PLANK "models/beam/plank.obj"
#define TYPE_FLOOR "models/beam/floor.obj"

#define POLE_SIZE_X		2.0f
#define POLE_SIZE_Y		2.0f
#define POLE_SIZE_Z		24.0f

#define PLANK_SIZE_X	1.0f 
#define PLANK_SIZE_Y	10.0f
#define PLANK_SIZE_Z	20.0f

#define FLOOR_SIZE_X	1.0f
#define FLOOR_SIZE_Y	30.0f
#define FLOOR_SIZE_Z	30.0f

#define BEAM_SIZE_X		4.0f
#define BEAM_SIZE_Y		4.0f
#define BEAM_SIZE_Z		12.0f

#include "gameobject.h"
#include "gameobjectcomponent.h"
#include "objectmodel.h"
#include "reactphysics3d.h"
#include "physics.h"

class Block : public GameObject
{
public:
	Block(rp3d::DynamicsWorld* physicsWorld, string type, vec::Vec3f position, vec::Vec3f rotation, vec::Vec3f size, float mass, bool isStatic);
	~Block() {};
};

class BlockDrawComponent : public DrawComponent
{
private:
	ObjModel * model;

public:
	void update(float elapsedSeconds) {}
};
